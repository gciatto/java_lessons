package it.unibo.java.lessons.expressions;

import static org.junit.Assert.*;

import static it.unibo.java.lessons.expressions.Expressions.*;

import javax.swing.JOptionPane;

import org.junit.Test;

public class TestExpressions {
	private static final String FORMAT = "Evaluate the following expression\n%s";

//	@Test
//	public void testIntExp1() {
//		final String inputStr = JOptionPane.showInputDialog(String.format(FORMAT, INT_EXP_STR_1));
//		try {
//			final int input = Integer.parseInt(inputStr);
//			assertTrue(input == INT_EXP_1);
//		} catch (Throwable e) {
//			fail();
//		}
//	}
//	
//	@Test
//	public void testIntExp2() {
//		final String inputStr = JOptionPane.showInputDialog(String.format(FORMAT, INT_EXP_STR_2));
//		try {
//			final int input = Integer.parseInt(inputStr);
//			assertTrue(input == INT_EXP_2);
//		} catch (Throwable e) {
//			fail();
//		}
//	}
	
	@Test
	public void testIntExp2Result() {
		try {
			final int input = Expressions.intExp2Result();
			assertTrue(input == INT_EXP_2);
		} catch (Throwable e) {
			fail();
		}
	}
	
	@Test
	public void testIntExp1Result() {
		try {
			final int input = Expressions.intExp1Result();
			assertTrue(input == INT_EXP_1);
		} catch (Throwable e) {
			fail();
		}
	}

}
