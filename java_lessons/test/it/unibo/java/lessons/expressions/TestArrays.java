package it.unibo.java.lessons.expressions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import javax.swing.JOptionPane;

import static it.unibo.java.lessons.arrays.Arrays.*;

import org.junit.Before;
import org.junit.Test;

import it.unibo.gciatto.utils.Utils;

public class TestArrays {

	private int[] array1, array2;

	@Before
	public void setUp() throws Exception {
		array1 = new int[Utils.randInt(10, 20)];
		array2 = new int[array1.length];
		for (int i = 0; i < array1.length; i++) {
			array1[i] = Utils.randInt(0, array1.length);
			array2[i] = Utils.randInt(0, array1.length);
		}
	}

	@Test
	public void testInvert() {
		final int[] copy = Arrays.copyOf(array1, array1.length);
		invert(array1);
		JOptionPane.showMessageDialog(null, 
					"testInvert\n" +
							"array1=" + Arrays.toString(copy) + "\n" +
							"after invert(array1)\narray1=" + Arrays.toString(array1)
				);
		assertNotNull(array1);
		assertTrue(array1.length == copy.length);
		assertTrue(IntStream.range(0, array1.length).allMatch(i -> array1[i] == copy[array1.length - 1 - i]));

	}
	
	@Test
	public void testInverted() {
		final int[] inverted = inverted(array1);
		JOptionPane.showMessageDialog(null, 
					"testInverted\n" +
							"array1=" + Arrays.toString(array1) + "\n" +
							"inverted(array1)=" + Arrays.toString(inverted)
				);
		assertNotNull(inverted);
		assertTrue(array1.length == inverted.length);
		assertTrue(IntStream.range(0, array1.length).allMatch(i -> array1[i] == inverted[array1.length - 1 - i]));

	}
	
	@Test
	public void testEvenElements() {
		final int[] evenElements = evenElements(array1);
		JOptionPane.showMessageDialog(null, 
					"testEvenElements\n" +
							"array1=" + Arrays.toString(array1) + "\n" +
							"evenElements(array1)=" + Arrays.toString(evenElements)
				);
		assertNotNull(evenElements);
		assertTrue(evenElements.length == (array1.length + 1) / 2);
		assertTrue(IntStream.rangeClosed(0, evenElements.length)
			.filter(i -> i % 2 == 0)
			.mapToObj(i -> new int[] { i, array1[i] })
			.allMatch(couple -> 
				couple[1] == array1[couple[0]]
			));

	}

	@Test
	public void testScalarProduct() {
		final int scalarProduct = scalarProduct(array1, array2);
		JOptionPane.showMessageDialog(null, 
					"testScalarProduct\n" +
							"array1=" + Arrays.toString(array1) + "\n" +
							"array2=" + Arrays.toString(array2) + "\n" +
							"scalarProduct(array1,array2)=" + scalarProduct
				);
		assertTrue(scalarProduct == IntStream.rangeClosed(0, array1.length)
			.map(i -> array1[i] * array2[i])
			.reduce(Integer::sum).getAsInt());

	}
}
