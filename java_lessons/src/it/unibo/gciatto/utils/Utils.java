package it.unibo.gciatto.utils;

import java.util.Random;

public class Utils {
	private static final Random R = new Random();
	
	public static int randInt(int min, int max) {
		return min + R.nextInt(max - min);
	}
}
