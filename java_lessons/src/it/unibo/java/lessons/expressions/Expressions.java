package it.unibo.java.lessons.expressions;

@SuppressWarnings("all")
public class Expressions {
	public static String INT_EXP_STR_1 = "(1 << 5) % 2 == 0 ? 10 / 2 / 6 * 2 + 1 % 2 - 3 * (1 & 2 | 3) / -1 : (7 & 4 | 1)";
	public static String INT_EXP_STR_2 = "((1 << 5) & 1) != 0 ? 10 / 2 / 6 * 2 + 1 % 2 - 3 * (1 & 2 | 3) / -1 : (7 & 4 | 1)";
	public static int INT_EXP_1 = (1 << 5) % 2 == 0 ? 10 / 2 / 6 * 2 + 1 % 2 - 3 * (1 & 2 | 3) / -1 : (7 & 4 | 1);
	public static int INT_EXP_2 = ((1 << 5) & 1) != 0 ? 10 / 2 / 6 * 2 + 1 % 2 - 3 * (1 & 2 | 3) / -1 : (7 & 4 | 1);

	public static int intExp1Result() {
		return -1;
	}
	
	public static int intExp2Result() {
		return -1;
	}
}
